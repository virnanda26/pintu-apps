# pintu-apps
A simple apps build with love.

**Note: This CI/CD Pipelines is devoted to deploy the apps on Google Kubernetes Engine cluster. In order to do that, please prepare your Google service account keys.<br>

## Prerequisites
- GKE cluster
- Google serive account keys (If you don't have one, please refer to this [docs](https://cloud.google.com/iam/docs/creating-managing-service-account-keys) to create)

## How To Deploy
1. Go to **CI/CD** and select **Pipelines**.
2. Click on **Run Pipeline**, or you can jump [here](https://gitlab.com/rivaldyarif/bmri-echoserver/-/pipelines/new).
3. On the **Run Pipeline** page, select branch `main`.<br>
4. Click **Run Pipeline**.
5. Go to **CI/CD Pipelines** and click on your running pipeline, simply click [here](https://gitlab.com/rivaldyarif/bmri-echoserver/-/pipelines).
6. To verify the apps, click on the **Output** job and copy the adress you get on the `kubectl get ingress` side then paste the address in the New tab.
```
$ kubectl get ingress
NAME                      CLASS    HOSTS   ADDRESS         PORTS   AGE
nanda-pintu-apps-ingress   <none>   *       34.117.177.26   80      2m
```
8. Note: It might take a few minutes for GKE to allocate an external IP address and set up forwarding rules before the load balancer is ready to serve your application. You might get errors such as HTTP 404 or HTTP 500 until the load balancer configuration is propagated across the globe.
